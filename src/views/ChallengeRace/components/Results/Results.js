/* eslint-disable react/no-multi-comp */
import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  Button,
  Link,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from '@material-ui/core';

import { GenericMoreButton, TableEditBar } from 'components';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 700
  },
  nameCell: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  actions: {
    padding: theme.spacing(1),
    justifyContent: 'flex-end'
  }
}));

const Info = ({ challenge, seasons }) => {
  const season = seasons.find(season => season.challenge.id === challenge.id);
  if (season) {
    if (season.passed) {
      return (
        <Button
          color="primary"
          component={RouterLink}
          size="small"
          to={`/races/${season.id}`}
          variant="contained">
          Passed
        </Button>
      );
    } else {
      return (
        <Button
          color="secondary"
          component={RouterLink}
          size="small"
          to={`/races/${season.id}`}
          variant="contained">
          On going
        </Button>
      );
    }
  } else {
    return (
      <Button color="secondary" size="small" variant="contained" disabled>
        Not Pass
      </Button>
    );
  }
};

const Results = props => {
  const { className, challenges, seasons, ...rest } = props;
  const classes = useStyles();

  // eslint-disable-next-line no-unused-vars
  const [selectedChallenges, setSelectedChallenges] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(25);

  const handleChangePage = (event, page) => {
    setPage(page);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(event.target.value);
  };

  return (
    <div {...rest} className={clsx(classes.root, className)}>
      <Typography color="textSecondary" gutterBottom variant="body2">
        {challenges.length} Records found. Page {page + 1} of{' '}
        {Math.ceil(challenges.length / rowsPerPage)}
      </Typography>
      <Card>
        <CardHeader action={<GenericMoreButton />} title="All challenges" />
        <Divider />
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell align="right">Info</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {challenges
                    .slice(page * rowsPerPage, Math.min((page + 1) * rowsPerPage, challenges.length))
                    .map(challenge => (
                      <TableRow
                        hover
                        key={challenge.id}
                        selected={
                          selectedChallenges.indexOf(challenge.id) !== -1
                        }>
                        <TableCell>
                          <div className={classes.nameCell}>
                            <div>
                              <Link color="inherit" variant="h6">
                                {challenge.name}
                              </Link>
                            </div>
                          </div>
                        </TableCell>
                        <TableCell align="right">
                          <Info challenge={challenge} seasons={seasons} />
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </div>
          </PerfectScrollbar>
        </CardContent>
        <CardActions className={classes.actions}>
          <TablePagination
            component="div"
            count={challenges.length}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
            page={page}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={[5, 10, 25]}
          />
        </CardActions>
      </Card>
      <TableEditBar selected={selectedChallenges} />
    </div>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  challenges: PropTypes.array.isRequired
};

Results.defaultProps = {
  challenges: []
};

export default Results;
