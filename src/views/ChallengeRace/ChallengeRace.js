import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';

import { Page, SearchBar } from 'components';
import { Header, Results } from './components';
import challengeService from '../../services/challenge.service';
import functionService from '../../services/function.service';
import { useSelector } from 'react-redux';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  }
}));

const ChallengeManagementList = () => {
  const classes = useStyles();
  const user = useSelector(s => s.session.user);
  const [seasons, setSeasons] = useState([]);
  const [challenges, setChallenges] = useState([]);

  useEffect(() => {
    const fetchChallenges = async () => {
      const challenges = await challengeService.getChallenges();
      const seasons = await functionService.getSeasons(user.id);
      setSeasons(seasons);
      setChallenges(challenges);
    };
    fetchChallenges();
    return () => {};
  }, [user.id]);

  const handleFilter = () => {};
  const handleSearch = () => {};

  return (
    <Page className={classes.root} title="Challenge Management List">
      <Header />
      <SearchBar onFilter={handleFilter} onSearch={handleSearch} />
      {challenges && (
        <Results
          className={classes.results}
          challenges={challenges}
          seasons={seasons}
        />
      )}
    </Page>
  );
};

export default ChallengeManagementList;
