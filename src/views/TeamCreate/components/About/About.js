import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem
} from '@material-ui/core';
import { Button } from '@material-ui/core';
import { useParams } from 'react-router';
import teamService from '../../../../services/team.service';
import useRouter from 'utils/useRouter';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 240
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));

const About = props => {
  const { className, ...rest } = props;
  const classes = useStyles();
  const router = useRouter();
  const { id } = useParams();
  const isCreate = id ? false : true;

  const initialValues = {
    name: '',
    username: '',
    password: '',
    color: '',
    role: 'team'
  };

  const [values, setValues] = useState({ ...initialValues });
  const [isUpdatePassword, setIsUpdatePassword] = useState(false);

  useEffect(() => {
    if (!isCreate) {
      const fetchTeam = async () => {
        const team = await teamService.getTeam(id);
        setValues(team);
      };

      fetchTeam();
    }

    return () => {};
  }, [id, isCreate]);

  const handleSubmit = async e => {
    e.preventDefault();

    if (isCreate) {
      await teamService.createTeam(values);
    } else {
      await teamService.updateTeam(values);

      if (isUpdatePassword) {
        await teamService.changePassword(values);
      }
    }

    router.history.push('/management/teams');
  };

  const handleFieldChange = (event, field, value) => {
    event.persist && event.persist();

    if (field === 'password') {
      setIsUpdatePassword(true);
    }

    setValues(values => ({
      ...values,
      [field]: value
    }));
  };

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <CardContent>
        <form onSubmit={handleSubmit}>
          <div className={classes.formGroup}>
            <TextField
              required
              fullWidth
              label="Name"
              name="name"
              onChange={event =>
                handleFieldChange(event, 'name', event.target.value)
              }
              value={values.name}
              variant="outlined"
            />
          </div>
          <div className={classes.formGroup}>
            <TextField
              required
              fullWidth
              label="Username"
              name="username"
              onChange={event =>
                handleFieldChange(event, 'username', event.target.value)
              }
              value={values.username}
              variant="outlined"
            />
          </div>
          <div className={classes.formGroup}>
            <TextField
              required
              fullWidth
              label="Password"
              name="password"
              type="password"
              onChange={event =>
                handleFieldChange(event, 'password', event.target.value)
              }
              value={values.password}
              variant="outlined"
            />
          </div>
          <div className={classes.formGroup}>
            <FormControl required className={classes.formControl}>
              <InputLabel>Role</InputLabel>
              <Select
                value={values.role}
                onChange={event =>
                  handleFieldChange(event, 'role', event.target.value)
                }
                className={classes.selectEmpty}>
                <MenuItem value={'team'}>Team</MenuItem>
                <MenuItem value={'admin'}>Admin</MenuItem>
              </Select>
            </FormControl>
          </div>
          <div className={classes.actions} style={{ flexDirection: 'row' }}>
            <Button
              color="primary"
              variant="contained"
              type="submit"
              style={{ marginRight: 12 }}>
              Save
            </Button>
            <Button
              color="secondary"
              variant="contained"
              onClick={() => router.history.goBack()}>
              Back
            </Button>
          </div>
        </form>
      </CardContent>
    </Card>
  );
};

About.propTypes = {
  className: PropTypes.string
};

export default About;
