import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  Button,
  Link,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from '@material-ui/core';

import { GenericMoreButton, TableEditBar } from 'components';
import teamService from 'services/team.service';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 700
  },
  nameCell: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  actions: {
    padding: theme.spacing(1),
    justifyContent: 'flex-end'
  }
}));

const Results = props => {
  const { className, ...rest } = props;
  const t = props.teams;
  const classes = useStyles();

  const [teams, setTeams] = useState(props.teams);
  // eslint-disable-next-line no-unused-vars
  const [selectedTeams, setSelectedTeams] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(25);

  const handleChangePage = (event, page) => {
    setPage(page);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(event.target.value);
  };

  useEffect(() => {
    setTeams(t);
  }, [t]);

  const handleDelete = async id => {
    await teamService.deleteTeam(id);
    setTeams(teams.filter(t => t.id !== id));
  };

  return (
    <div {...rest} className={clsx(classes.root, className)}>
      <Typography color="textSecondary" gutterBottom variant="body2">
        {teams.length} Records found. Page {page + 1} of{' '}
        {Math.ceil(teams.length / rowsPerPage)}
      </Typography>
      <Card>
        <CardHeader action={<GenericMoreButton />} title="All teams" />
        <Divider />
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Role</TableCell>
                    <TableCell align="right">Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {teams
                    .slice(
                      page * rowsPerPage,
                      Math.min((page + 1) * rowsPerPage, teams.length)
                    )
                    .map(team => (
                      <TableRow
                        hover
                        key={team.id}
                        selected={selectedTeams.indexOf(team.id) !== -1}>
                        <TableCell>
                          <div className={classes.nameCell}>
                            <div>
                              <Link
                                color="inherit"
                                component={RouterLink}
                                to={`/management/teams/${team.id}`}
                                variant="h6">
                                {team.name}
                              </Link>
                              <div>{team.username}</div>
                            </div>
                          </div>
                        </TableCell>
                        <TableCell>{team.role.toUpperCase()}</TableCell>
                        <TableCell align="right">
                          <Button
                            color="primary"
                            component={RouterLink}
                            size="small"
                            to={`/management/teams/${team.id}`}
                            variant="outlined">
                            Edit
                          </Button>
                          <Button
                            color="secondary"
                            onClick={() => handleDelete(team.id)}
                            component={RouterLink}
                            size="small"
                            variant="outlined">
                            Delete
                          </Button>
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </div>
          </PerfectScrollbar>
        </CardContent>
        <CardActions className={classes.actions}>
          <TablePagination
            component="div"
            count={teams.length}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
            page={page}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={[5, 10, 25]}
          />
        </CardActions>
      </Card>
      <TableEditBar selected={selectedTeams} />
    </div>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  teams: PropTypes.array.isRequired
};

Results.defaultProps = {
  teams: []
};

export default Results;
