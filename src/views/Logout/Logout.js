import React from 'react';
import { useDispatch } from 'react-redux';
import { logout } from 'actions';
import useRouter from 'utils/useRouter';

const Logout = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  const handleLogout = () => {
    router.history.push('/auth/login');
    dispatch(logout());
  };

  React.useEffect(() => {
    handleLogout();
    // eslint-disable-next-line
  }, []);

  return <div />;
};

export default Logout;
