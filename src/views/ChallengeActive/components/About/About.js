import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Card, CardContent, TextField, Typography } from '@material-ui/core';
import { Button } from '@material-ui/core';
import { useParams } from 'react-router';
import functionService from '../../../../services/function.service';
import teamService from '../../../../services/team.service';
import useRouter from 'utils/useRouter';
import { EditorState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import { convertFromRaw } from 'draft-js';
import { ToastContainer, toast } from 'react-toastify';
import { useSelector } from 'react-redux';
import moment from 'moment';
import 'react-toastify/dist/ReactToastify.css';
import '../../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const About = props => {
  const { className, ...rest } = props;
  const classes = useStyles();
  const router = useRouter();
  const { id } = useParams();
  const teamId = useSelector(s => s.session.user.id);

  const initialValues = {
    key: ''
  };

  const [counter, setCounter] = React.useState(0);
  const [content, setContent] = useState(EditorState.createEmpty());
  const [hint, setHint] = useState(EditorState.createEmpty());
  const [showHint, setShowHint] = useState(false);
  const [values, setValues] = useState({ ...initialValues });
  const [season, setSeason] = useState({ passed: false, showHint: false });

  useEffect(() => {
    if (counter > 0) {
      setTimeout(() => setCounter(counter - 1), 1000);
    } else {
      setCounter(0);
    }
  }, [counter]);

  useEffect(() => {
    const fetchChallenge = async () => {
      const season = await functionService.getSeason(id);
      setSeason(season);

      await updateCounter();

      try {
        setContent(
          EditorState.createWithContent(
            convertFromRaw(JSON.parse(season.challenge.content))
          )
        );
        setHint(
          EditorState.createWithContent(
            convertFromRaw(JSON.parse(season.challenge.hint))
          )
        );
        // eslint-disable-next-line no-empty
      } catch (error) {}
    };

    fetchChallenge();

    return () => {};
    // eslint-disable-next-line
  }, [id]);

  const updateCounter = async () => {
    const team = await teamService.getTeam(teamId);
    const duration = moment
      .duration(moment(team.preventDate).diff(moment()))
      .asSeconds();

    if (duration > 0) {
      setCounter(duration);
    }

    const hintDuration = moment
      .duration(moment().diff(team.lastTime))
      .asSeconds();

    if (hintDuration > 10 * 60) {
      setShowHint(true);
    } else {
      setTimeout(() => setShowHint(true), (10 * 60 - hintDuration) * 1000);
    }

    return duration;
  };

  const notify = message => {
    toast.error(message, {
      position: 'top-right',
      autoClose: 5000
    });
  };

  const handleSubmit = async e => {
    e.preventDefault();
    const duration = await updateCounter();

    if (duration <= 0 || !duration) {
      const isSuccess = await functionService.active(id, values.key);

      if (isSuccess) {
        router.history.push('/races');
      } else {
        notify('Wrong key!!!');
        await updateCounter();
      }
    } else {
      notify('Please wait to end of countdown!!!');
    }
  };

  const handleFieldChange = (event, field, value) => {
    event.persist && event.persist();

    setValues(values => ({
      ...values,
      [field]: value
    }));
  };

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <CardContent>
        <form onSubmit={handleSubmit}>
          <div className={classes.formGroup}>
            <Typography variant="h4">Content</Typography>
            <Editor
              readOnly={true}
              toolbarHidden={true}
              editorState={content}
              wrapperClassName="demo-wrapper"
              editorClassName="demo-editor"
              onEditorStateChange={setContent}
            />
          </div>
          {season.showHint || showHint ? (
            <div className={classes.formGroup}>
              <Typography variant="h4">Hint</Typography>
              <Editor
                readOnly={true}
                toolbarHidden={true}
                editorState={hint}
                wrapperClassName="demo-wrapper"
                editorClassName="demo-editor"
                onEditorStateChange={setHint}
              />
            </div>
          ) : (
            <div className={classes.formGroup} />
          )}

          {!season.passed ? (
            <div className={classes.formGroup}>
              <TextField
                required
                fullWidth
                label="Key"
                name="key"
                type="string"
                onChange={event =>
                  handleFieldChange(event, 'key', event.target.value)
                }
                value={values.key}
                variant="outlined"
              />
            </div>
          ) : (
            <div className={classes.formGroup} />
          )}
          <div className={classes.formGroup} style={{ flexDirection: 'row' }}>
            {counter ? (
              <Button
                color="primary"
                variant="contained"
                disabled
                style={{ marginRight: 12 }}>
                {moment
                  .utc(moment.duration(counter, 'seconds').as('milliseconds'))
                  .format('HH:mm:ss')}
              </Button>
            ) : (
              <Button
                color="primary"
                variant="contained"
                type="submit"
                disabled={season.passed}
                style={{ marginRight: 12 }}>
                Active
              </Button>
            )}
            <Button
              color="secondary"
              variant="contained"
              onClick={() => router.history.goBack()}>
              Back
            </Button>
          </div>
        </form>
      </CardContent>
      <ToastContainer />
    </Card>
  );
};

About.propTypes = {
  className: PropTypes.string
};

export default About;
