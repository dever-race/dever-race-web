import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';

import { Page, SearchBar } from 'components';
import { Header, Results } from './components';
import teamService from '../../services/team.service';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  }
}));

const TeamManagementList = () => {
  const classes = useStyles();

  const [teams, setTeams] = useState([]);

  useEffect(() => {
    const fetchTeams = async () => {
      const teams = await teamService.getTeamRankings();

      setTeams(teams);
    };
    fetchTeams();
    return () => {};
  }, []);

  const handleFilter = () => {};
  const handleSearch = () => {};

  return (
    <Page className={classes.root} title="Team Management List">
      <Header />
      <SearchBar onFilter={handleFilter} onSearch={handleSearch} />
      {teams && <Results className={classes.results} teams={teams} />}
    </Page>
  );
};

export default TeamManagementList;
