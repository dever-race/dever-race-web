import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Card, CardContent, TextField, Typography } from '@material-ui/core';
import { Button } from '@material-ui/core';
import { useParams } from 'react-router';
import challengeService from '../../../../services/challenge.service';
import useRouter from 'utils/useRouter';
import { EditorState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import { convertFromRaw, convertToRaw } from 'draft-js';
import '../../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const About = props => {
  const { className, ...rest } = props;
  const classes = useStyles();
  const router = useRouter();
  const { id } = useParams();
  const isCreate = id ? false : true;

  const initialValues = {
    name: '',
    location: '',
    order: '',
    key: ''
  };

  const [content, setContent] = useState(EditorState.createEmpty());
  const [hint, setHint] = useState(EditorState.createEmpty());
  const [values, setValues] = useState({ ...initialValues });

  useEffect(() => {
    if (!isCreate) {
      const fetchChallenge = async () => {
        const challenge = await challengeService.getChallenge(id);
        setValues(challenge);

        try {
          setContent(
            EditorState.createWithContent(
              convertFromRaw(JSON.parse(challenge.content))
            )
          );
          setHint(
            EditorState.createWithContent(
              convertFromRaw(JSON.parse(challenge.hint))
            )
          );
        } catch (error) {}
      };

      fetchChallenge();
    }

    return () => {};
  }, [id, isCreate]);

  const handleSubmit = async e => {
    e.preventDefault();
    values.content = JSON.stringify(convertToRaw(content.getCurrentContent()));
    values.hint = JSON.stringify(convertToRaw(hint.getCurrentContent()));

    if (isCreate) {
      await challengeService.createChallenge(values);
    } else {
      await challengeService.updateChallenge(values);
    }

    router.history.push('/management/challenges');
  };

  const handleFieldChange = (event, field, value) => {
    event.persist && event.persist();

    setValues(values => ({
      ...values,
      [field]: value
    }));
  };

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <CardContent>
        <form onSubmit={handleSubmit}>
          <div className={classes.formGroup}>
            <TextField
              required
              fullWidth
              label="Name"
              name="name"
              onChange={event =>
                handleFieldChange(event, 'name', event.target.value)
              }
              value={values.name}
              variant="outlined"
            />
          </div>
          <div className={classes.formGroup}>
            <TextField
              required
              fullWidth
              label="Location"
              name="location"
              onChange={event =>
                handleFieldChange(event, 'location', event.target.value)
              }
              value={values.location}
              variant="outlined"
            />
          </div>
          <div className={classes.formGroup}>
            <TextField
              required
              fullWidth
              label="Key"
              name="key"
              onChange={event =>
                handleFieldChange(event, 'key', event.target.value)
              }
              value={values.key}
              variant="outlined"
            />
          </div>
          <div className={classes.formGroup}>
            <Typography variant="h6">Content</Typography>
            <br />
            <Editor
              editorState={content}
              wrapperClassName="demo-wrapper"
              editorClassName="demo-editor"
              onEditorStateChange={setContent}
            />
          </div>
          <div className={classes.formGroup}>
            <Typography variant="h6">Hint</Typography>
            <br />
            <Editor
              editorState={hint}
              wrapperClassName="demo-wrapper"
              editorClassName="demo-editor"
              onEditorStateChange={setHint}
            />
          </div>
          <div className={classes.actions} style={{ flexDirection: 'row' }}>
            <Button
              color="primary"
              variant="contained"
              type="submit"
              style={{ marginRight: 12 }}>
              Save
            </Button>
            <Button
              color="secondary"
              variant="contained"
              onClick={() => router.history.goBack()}>
              Back
            </Button>
          </div>
        </form>
      </CardContent>
    </Card>
  );
};

About.propTypes = {
  className: PropTypes.string
};

export default About;
