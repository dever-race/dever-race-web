import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';

import { Page, SearchBar } from 'components';
import { Header, Results } from './components';
import challengeService from '../../services/challenge.service';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  }
}));

const ChallengeManagementList = () => {
  const classes = useStyles();

  const [challenges, setChallenges] = useState([]);

  useEffect(() => {
    const fetchChallenges = async () => {
      const challenges = await challengeService.getChallenges();
      setChallenges(challenges);
    };
    fetchChallenges();
    return () => {};
  }, []);

  const handleFilter = () => {};
  const handleSearch = () => {};

  return (
    <Page className={classes.root} title="Challenge Management List">
      <Header />
      <SearchBar onFilter={handleFilter} onSearch={handleSearch} />
      {challenges && <Results className={classes.results} challenges={challenges} />}
    </Page>
  );
};

export default ChallengeManagementList;
