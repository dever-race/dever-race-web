import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  Button,
  Link,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from '@material-ui/core';

import { GenericMoreButton, TableEditBar } from 'components';
import challengeService from 'services/challenge.service';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 700
  },
  nameCell: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  actions: {
    padding: theme.spacing(1),
    justifyContent: 'flex-end'
  }
}));

const Results = props => {
  const { className, ...rest } = props;
  const t = props.challenges;
  const classes = useStyles();

  const [challenges, setChallenges] = useState(props.challenges);
  // eslint-disable-next-line no-unused-vars
  const [selectedChallenges, setSelectedChallenges] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(25);

  const handleChangePage = (event, page) => {
    setPage(page);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(event.target.value);
  };

  useEffect(() => {
    setChallenges(t);
  }, [t]);

  const handleDelete = async id => {
    await challengeService.deleteChallenge(id);
    setChallenges(challenges.filter(t => t.id !== id));
  };

  const handleUpDown = async (id, isUp) => {
    let second = challenges.indexOf(challenges.find(c => c.id === id));
    second = isUp ? second : second + 1;

    if (second > 0 && second < challenges.length) {
      const first = second - 1;

      const challenges = await challengeService.updateOrder(first, second);
      setChallenges(challenges);
    }
  };

  return (
    <div {...rest} className={clsx(classes.root, className)}>
      <Typography color="textSecondary" gutterBottom variant="body2">
        {challenges.length} Records found. Page {page + 1} of{' '}
        {Math.ceil(challenges.length / rowsPerPage)}
      </Typography>
      <Card>
        <CardHeader action={<GenericMoreButton />} title="All challenges" />
        <Divider />
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Location</TableCell>
                    <TableCell>Key</TableCell>
                    <TableCell align="right">Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {challenges
                    .slice(
                      page * rowsPerPage,
                      Math.min((page + 1) * rowsPerPage, challenges.length)
                    )
                    .map(challenge => (
                      <TableRow
                        hover
                        key={challenge.id}
                        selected={
                          selectedChallenges.indexOf(challenge.id) !== -1
                        }>
                        <TableCell>
                          <div className={classes.nameCell}>
                            <div>
                              <Link
                                color="inherit"
                                component={RouterLink}
                                to={`/management/challenges/${challenge.id}`}
                                variant="h6">
                                {challenge.name}
                              </Link>
                            </div>
                          </div>
                        </TableCell>
                        <TableCell>{challenge.location}</TableCell>
                        <TableCell>{challenge.key}</TableCell>
                        <TableCell align="right">
                          <Button
                            color="default"
                            onClick={() => handleUpDown(challenge.id, true)}
                            size="small"
                            style={{ margin: 4 }}
                            variant="contained">
                            Up
                          </Button>
                          <Button
                            color="inherit"
                            onClick={() => handleUpDown(challenge.id, false)}
                            size="small"
                            style={{ margin: 4 }}
                            variant="contained">
                            Down
                          </Button>
                          <Button
                            color="primary"
                            component={RouterLink}
                            size="small"
                            to={`/management/challenges/${challenge.id}`}
                            style={{ margin: 4 }}
                            variant="contained">
                            Edit
                          </Button>
                          <Button
                            color="secondary"
                            onClick={() => handleDelete(challenge.id)}
                            size="small"
                            style={{ margin: 4 }}
                            variant="contained">
                            Delete
                          </Button>
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </div>
          </PerfectScrollbar>
        </CardContent>
        <CardActions className={classes.actions}>
          <TablePagination
            component="div"
            count={challenges.length}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
            page={page}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={[5, 10, 25]}
          />
        </CardActions>
      </Card>
      <TableEditBar selected={selectedChallenges} />
    </div>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  challenges: PropTypes.array.isRequired
};

Results.defaultProps = {
  challenges: []
};

export default Results;
