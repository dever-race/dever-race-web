import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  Select,
  FormControl,
  MenuItem,
  InputLabel
} from '@material-ui/core';
import { Button } from '@material-ui/core';
import { useSelector } from 'react-redux';
import useRouter from 'utils/useRouter';

import functionService from '../../../../services/function.service';
import teamService from '../../../../services/team.service';
import challengeService from '../../../../services/challenge.service';
import moment from 'moment';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 240
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));

const About = props => {
  const { className, ...rest } = props;
  const classes = useStyles();
  const router = useRouter();
  const teamId = useSelector(s => s.session.user.id);

  const [counter, setCounter] = React.useState(0);
  const [helper, setHelper] = React.useState('');
  const [helpers, setHelpers] = React.useState([]);
  const [destination, setDestination] = React.useState('');
  const [destinations, setDestinations] = React.useState([]);
  const [submitDisable, setSubmitDisable] = React.useState(true);

  useEffect(() => {
    if (counter > 0) {
      setTimeout(() => setCounter(counter - 1), 1000);
    } else {
      setCounter(0);
    }
  }, [counter]);

  useEffect(() => {
    const fetchHelpers = async () => {
      updateCounter();

      const team = await teamService.getTeam(teamId);
      const helpers = [];
      if (team.hintHelper > 0)
        helpers.push({
          value: 'HINT',
          description: `Show hint for a challenge (${team.hintHelper})`
        });
      if (team.skipHelper > 0)
        helpers.push({
          value: 'SKIP',
          description: `Skip a challenge (${team.skipHelper})`
        });
      if (team.openHelper > 0)
        helpers.push({
          value: 'OPEN',
          description: `Open one more challenge (${team.openHelper})`
        });
      if (team.preventHelper > 0)
        helpers.push({
          value: 'PREVENT',
          description: `Prevent a team (${team.preventHelper})`
        });

      setHelpers(helpers);
    };

    fetchHelpers();
    return () => {};
    // eslint-disable-next-line
  }, [teamId]);

  useEffect(() => {
    setSubmitDisable(!helper.length || !destination.length);

    return () => {};
  }, [helper, destination]);

  const updateCounter = async () => {
    const team = await teamService.getTeam(teamId);
    const duration = moment
      .duration(moment(team.preventDate).diff(moment()))
      .asSeconds();

    if (duration > 0) {
      setCounter(duration);
    }

    return duration;
  };

  const handleHelper = async event => {
    setHelper(event.target.value);
    setDestination('');

    let destinations = [];
    // eslint-disable-next-line
    switch (event.target.value) {
      case 'HINT':
      case 'SKIP':
        {
          const seasons = await functionService.getSeasons(teamId);
          destinations = seasons
            .filter(s => !s.passed)
            .map(s => {
              return {
                value: s.challenge.id,
                description: s.challenge.name
              };
            });
        }
        break;
      case 'OPEN':
        {
          const seasons = await functionService.getSeasons(teamId);
          const openedChallenges = seasons.map(s => s.challenge.id);
          const challenges = await challengeService.getChallenges();
          destinations = challenges
            .filter(x => !openedChallenges.includes(x.id))
            .map(x => {
              return {
                value: x.id,
                description: x.name
              };
            });
        }
        break;
      case 'PREVENT':
        {
          const teams = await teamService.getTeams();
          destinations = teams
            .filter(x => x.id !== teamId)
            .map(x => {
              return {
                value: x.id,
                description: x.name
              };
            });
        }
        break;
    }
    setDestinations(destinations);
  };

  const handleDestination = event => {
    setDestination(event.target.value);
  };

  const handleSubmit = async e => {
    e.preventDefault();

    const duration = await updateCounter();

    if (duration <= 0 || !duration) {
      try {
        setSubmitDisable(true);
        await functionService.useHelper(helper, destination, teamId);

        router.history.push('/races');
      } catch (error) {
        setSubmitDisable(false);
      }
    } else {
      notify('Please wait to end of countdown!!!');
    }
  };

  const notify = message => {
    toast.error(message, {
      position: 'top-right',
      autoClose: 5000
    });
  };

  return (
    <Card {...rest} className={clsx(classes.root, className)}>
      <CardContent>
        <form onSubmit={handleSubmit}>
          <div className={classes.formGroup}>
            <FormControl required className={classes.formControl}>
              <InputLabel>Choose a helper</InputLabel>
              <Select
                value={helper}
                onChange={handleHelper}
                className={classes.selectEmpty}>
                {helpers.map(h => (
                  <MenuItem value={h.value} key={h.value}>
                    {h.description}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>
          {!helper.length ? (
            <div />
          ) : (
            <div className={classes.formGroup}>
              <FormControl required className={classes.formControl}>
                <InputLabel>Choose a destination</InputLabel>
                <Select
                  value={destination}
                  onChange={handleDestination}
                  className={classes.selectEmpty}>
                  {destinations.map(d => (
                    <MenuItem value={d.value} key={d.value}>
                      {d.description}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </div>
          )}
          <div className={classes.formGroup} style={{ flexDirection: 'row' }}>
            {counter ? (
              <Button
                color="primary"
                variant="contained"
                disabled
                style={{ marginRight: 12 }}>
                {moment
                  .utc(moment.duration(counter, 'seconds').as('milliseconds'))
                  .format('HH:mm:ss')}
              </Button>
            ) : (
              <Button
                color="primary"
                variant="contained"
                type="submit"
                disabled={submitDisable}
                style={{ marginRight: 12 }}>
                Submit
              </Button>
            )}
            <Button
              color="secondary"
              variant="contained"
              onClick={() => router.history.goBack()}>
              Back
            </Button>
          </div>
        </form>
      </CardContent>
      <ToastContainer />
    </Card>
  );
};

About.propTypes = {
  className: PropTypes.string
};

export default About;
