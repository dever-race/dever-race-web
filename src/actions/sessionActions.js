import authService from '../services/auth.service';

export const SESSION_LOGIN = 'SESSION_LOGIN';
export const SESSION_LOGOUT = 'SESSION_LOGOUT';

export const login = (username, password) => async dispatch => {
  const user = await authService.login(username, password);

  return dispatch({
    type: SESSION_LOGIN,
    user
  });
};

export const logout = () => async dispatch => {
  await authService.logout();
  dispatch({
    type: SESSION_LOGOUT
  });
};
