import axios from '../utils/axiosInstance';

const login = async (username, password) => {
  const { accessToken } = (await axios.post('/auth/login', {
    username,
    password
  })).data;
  if (accessToken) {
    localStorage.setItem('accessToken', accessToken);
  }

  const user = (await axios.get('/auth/profile')).data;
  if (user) {
    localStorage.setItem('user', JSON.stringify(user));
    return user;
  } else {
    return null;
  }
};

const logout = async () => {
  localStorage.clear();
};

export default {
  login,
  logout
};
