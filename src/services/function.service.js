import axios from '../utils/axiosSuper';

const getSeasons = async teamId => {
  return (await axios.get(`/seasons?filter=team.id||$eq||${teamId}`)).data;
};

const getSeason = async id => {
  return (await axios.get(`/seasons/${id}`)).data;
};

const active = async (id, key) => {
  return (await axios.patch(`/functions/active/${id}?key=${key}`)).data;
};

const useHelper = async (helper, destination, teamId) => {
  return await axios.patch(
    `/functions/helpers?helper=${helper}&destination=${destination}&teamId=${teamId}`
  );
};

export default {
  getSeasons,
  getSeason,
  active,
  useHelper
};
