import axios from '../utils/axiosSuper';

const getAllTeams = async () => {
  return (await axios.get('/teams?&sort=role,DESC')).data;
};

const getTeams = async () => {
  return (await axios.get('/teams?filter=role||$ne||admin')).data;
};

const getTeamRankings = async () => {
  return (await axios.get(
    '/teams?sort=passedChallenges,DESC&sort=lastTime,ASC&filter=role||$ne||admin'
  )).data;
};

const getTeam = async id => {
  return (await axios.get('/teams/' + id)).data;
};

const createTeam = async team => {
  return axios.post('/teams/', { ...team });
};

const updateTeam = async team => {
  return axios.patch('/teams/' + team.id, { ...team });
};

const deleteTeam = async id => {
  return axios.delete('/teams/' + id);
};

const changePassword = async ({ id, password }) => {
  return axios.patch(`/teams/${id}/changePassword?password=${password}`);
};

export default {
  getAllTeams,
  getTeams,
  getTeamRankings,
  getTeam,
  createTeam,
  updateTeam,
  changePassword,
  deleteTeam
};
