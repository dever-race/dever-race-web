import axios from '../utils/axiosSuper';

const getChallenges = async () => {
  return (await axios.get('/challenges?sort=order,ASC')).data;
};

const getChallenge = async id => {
  return (await axios.get('/challenges/' + id)).data;
};

const createChallenge = async challenge => {
  return axios.post('/challenges/', { ...challenge });
};

const updateChallenge = async challenge => {
  return axios.patch('/challenges/' + challenge.id, { ...challenge });
};

const updateOrder = async (first, second) => {
  return (await axios.put(
    `/challenges/updateOrder?first=${first}&second=${second}`
  )).data;
};

const deleteChallenge = async id => {
  return axios.delete('/challenges/' + id);
};

export default {
  getChallenges,
  getChallenge,
  createChallenge,
  updateChallenge,
  updateOrder,
  deleteChallenge
};
