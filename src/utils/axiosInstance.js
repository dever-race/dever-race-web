import axios from 'axios';

export const authHeader = () => {
  const token = localStorage.getItem('accessToken');

  if (token) {
    return { Authorization: `Bearer ${token}` };
  } else {
    return {};
  }
};

const callApi = (method, url, payload = {}, config = {}) => {
  const instance = axios.create({
    baseURL: process.env.REACT_APP_API_PATH,
    headers: authHeader()
  });

  switch (method) {
    case 'GET':
      return instance.get(url, config);
    case 'POST':
      return instance.post(url, payload, config);
    case 'PUT':
      return instance.put(url, payload, config);
    case 'PATCH':
      return instance.patch(url, payload, config);
    case 'DEL':
      return instance.delete(url, config);
    default:
      return null;
  }
};

export default {
  get: url => callApi('GET', url),
  post: (url, payload) => callApi('POST', url, payload),
  put: (url, payload) => callApi('PUT', url, payload),
  patch: (url, payload) => callApi('PATCH', url, payload),
  delete: url => callApi('DEL', url)
};
