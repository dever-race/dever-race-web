/* eslint-disable no-unused-vars */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/display-name */
import React from 'react';
import { colors } from '@material-ui/core';
import HomeIcon from '@material-ui/icons/HomeOutlined';
import GroupOutlinedIcon from '@material-ui/icons/GroupOutlined';
import GolfCourseOutlinedIcon from '@material-ui/icons/GolfCourseOutlined';
import AssistantOutlinedIcon from '@material-ui/icons/AssistantOutlined';
import FlagOutlinedIcon from '@material-ui/icons/FlagOutlined';
import FormatListNumberedOutlinedIcon from '@material-ui/icons/FormatListNumberedOutlined';
import ExitToAppOutlined from '@material-ui/icons/ExitToAppOutlined';

export default [
  {
    title: 'Pages',
    pages: [
      {
        title: 'Race',
        href: '/races',
        access: ['admin', 'team'],
        icon: FlagOutlinedIcon
      },
      {
        title: 'Ranking',
        href: '/ranking',
        access: ['admin', 'team'],
        icon: FormatListNumberedOutlinedIcon
      },
      {
        title: 'Helpers',
        href: '/helpers',
        access: ['admin', 'team'],
        icon: AssistantOutlinedIcon
      },
      {
        title: 'Challenge Management',
        href: '/management/challenges',
        access: ['admin'],
        icon: GolfCourseOutlinedIcon
      },
      {
        title: 'Team Management',
        href: '/management/teams',
        access: ['admin'],
        icon: GroupOutlinedIcon
      },
      {
        title: 'Logout',
        href: '/logout',
        access: ['admin', 'team'],
        icon: ExitToAppOutlined
      }
    ]
  }
];
