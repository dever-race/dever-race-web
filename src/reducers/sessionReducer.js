import * as actionTypes from 'actions';

const user = JSON.parse(localStorage.getItem('user')) || {
  first_name: 'Shen',
  last_name: 'Zhi',
  email: 'demo@devias.io',
  avatar: '/images/avatars/avatar_11.png',
  bio: 'Brain Director',
  role: 'guest' // ['GUEST', 'USER', 'ADMIN']
};

const initialState = {
  loggedIn: true,
  user
};

const sessionReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SESSION_LOGIN: {
      return {
        ...initialState,
        user: action.user
      };
    }

    case actionTypes.SESSION_LOGOUT: {
      return {
        ...state,
        loggedIn: false,
        user: {
          role: 'GUEST'
        }
      };
    }

    default: {
      return state;
    }
  }
};

export default sessionReducer;
