/* eslint-disable react/no-multi-comp */
/* eslint-disable react/display-name */
import React, { lazy } from 'react';
import { Redirect } from 'react-router-dom';

import AuthLayout from './layouts/Auth';
import ErrorLayout from './layouts/Error';
import DashboardLayout from './layouts/Dashboard';

const routes = [
  {
    path: '/',
    exact: true,
    component: () => <Redirect to="/races" />
  },
  {
    path: '/auth',
    component: AuthLayout,
    routes: [
      {
        path: '/auth/login',
        exact: true,
        component: lazy(() => import('views/Login'))
      },
      {
        path: '/auth/register',
        exact: true,
        component: lazy(() => import('views/Register'))
      },
      {
        component: () => <Redirect to="/errors/error-404" />
      }
    ]
  },
  {
    path: '/errors',
    component: ErrorLayout,
    routes: [
      {
        path: '/errors/error-401',
        exact: true,
        component: lazy(() => import('views/Error401'))
      },
      {
        path: '/errors/error-404',
        exact: true,
        component: lazy(() => import('views/Error404'))
      },
      {
        path: '/errors/error-500',
        exact: true,
        component: lazy(() => import('views/Error500'))
      },
      {
        component: () => <Redirect to="/errors/error-404" />
      }
    ]
  },
  {
    route: '*',
    component: DashboardLayout,
    routes: [
      {
        path: '/logout',
        exact: true,
        component: lazy(() => import('views/Logout'))
      },
      {
        path: '/helpers',
        exact: true,
        component: lazy(() => import('views/ChallengeHelper'))
      },
      {
        path: '/ranking',
        exact: true,
        component: lazy(() => import('views/Ranking'))
      },
      {
        path: '/races',
        exact: true,
        component: lazy(() => import('views/ChallengeRace'))
      },
      {
        path: '/races/:id',
        exact: true,
        component: lazy(() => import('views/ChallengeActive'))
      },
      {
        path: '/management/challenges',
        exact: true,
        component: lazy(() => import('views/ChallengeManagementList'))
      },
      {
        path: '/management/createChallenge/',
        exact: true,
        component: lazy(() => import('views/ChallengeCreate'))
      },
      {
        path: '/management/challenges/:id',
        exact: true,
        component: lazy(() => import('views/ChallengeCreate'))
      },
      {
        path: '/management/teams',
        exact: true,
        component: lazy(() => import('views/TeamManagementList'))
      },
      {
        path: '/management/createTeam/',
        exact: true,
        component: lazy(() => import('views/TeamCreate'))
      },
      {
        path: '/management/teams/:id',
        exact: true,
        component: lazy(() => import('views/TeamCreate'))
      }
    ]
  }
];

export default routes;
