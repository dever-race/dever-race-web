FROM node:12-alpine as build-stage

ADD package.json /tmp/package.json
RUN cd /tmp && yarn install
RUN mkdir -p /opt/app && cp -a /tmp/node_modules /opt/app/

WORKDIR /opt/app
ADD . /opt/app
RUN yarn build

FROM nginx:1.17

COPY --from=build-stage /opt/app/build/ /usr/share/nginx/html
COPY --from=build-stage /opt/app/nginx.conf /etc/nginx/conf.d/default.conf
ENTRYPOINT ["nginx", "-g", "daemon off;"]
